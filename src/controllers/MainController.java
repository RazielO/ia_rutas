package controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class MainController {
    @FXML
    Button btnRemoveCity, btnSave, btnAddCity;
    @FXML
    GridPane matrix;
    @FXML
    ListView<String> lvCities;
    @FXML
    TextField tfCityName;

    private ArrayList<String> cities;
    private int[][] citiesMatrix;

    /**
     * Se ejecuta cuando se carga la pantalla, inicializa las listas y coloca los listeners de
     * los botones
     */
    @FXML
    protected void initialize() {
        this.cities = new ArrayList<>();

        lvCities.setItems(FXCollections.observableArrayList(cities));
        createMatrix();

        btnAddCity.setOnMouseClicked(mouseEvent -> addCity());
        btnRemoveCity.setOnMouseClicked(mouseEvent -> removeCity());
        btnSave.setOnMouseClicked(mouseEvent -> save());
    }

    /**
     * Crea la matriz de forma visual, usa un GridPane y agrega Labels como encabezados
     * y TexFields para ingresar los pesos.
     */
    private void createMatrix() {
        getAllCities();
        matrix.getChildren().clear();
        for (int i = 0; i < cities.size() + 1; i++)
            for (int j = 0; j < cities.size() + 1; j++) {
                if (i == 0 && j == 0)
                    continue;
                else if (i == 0)
                    matrix.add(new Label(cities.get(j - 1)), j, i);
                else if (j == 0)
                    matrix.add(new Label(cities.get(i - 1)), j, i);
                else {
                    TextField aux = new TextField("0");
                    aux.setId((i - 1) + "," + (j - 1));
                    aux.setPrefWidth(60);
                    if (i == j)
                        aux.setEditable(false);

                    matrix.add(aux, j, i);
                }
            }
    }

    /**
     * Lee el contenido del ListView y lo guarda en un ArrayList
     */
    private void getAllCities() {
        this.cities = new ArrayList<>(this.lvCities.getItems());
    }

    /**
     * Lee el contenido de tfCityName, lo agrega a cities y a lvCities y llama al metodo createMatrix()
     */
    private void addCity() {
        this.cities.add(tfCityName.getText());
        tfCityName.setText("");
        lvCities.setItems(FXCollections.observableArrayList(cities));
        createMatrix();
    }

    /**
     * Elimina el contenido de cities, actualiza lvCities y llama al metodo createMatrix()
     */
    private void removeCity() {
        this.cities.remove(lvCities.getSelectionModel().getSelectedIndex());
        lvCities.setItems(FXCollections.observableArrayList(cities));
        createMatrix();
    }

    /**
     * Genera una matriz de enteros, si la lista y la matriz son distintos, llama al metodo alertMessage
     */
    private void save() {
        citiesMatrix = new int[this.cities.size()][this.cities.size()];

        for (Node node : matrix.getChildren()) {
            if (node.getTypeSelector().equals("TextField")) {
                TextField textField = (TextField) node;
                String[] id = textField.getId().split(",");
                citiesMatrix[Integer.parseInt(id[0])][Integer.parseInt(id[1])] = Integer.parseInt(textField.getText());
            }
        }
    }
}
