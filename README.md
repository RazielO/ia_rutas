# IA_Rutas

Genera matriz de forma automática según las ciudades que se agregen o se borren.

La matriz es llenada por el usuario con los pesos, la diagonal principal, no es editable, ya que la distancia entre una ciudad y ella misma, siempre será 0.

![demo](./demo/demo.gif)
